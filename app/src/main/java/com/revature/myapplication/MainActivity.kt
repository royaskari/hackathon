package com.revature.myapplication

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        LocalStorage.setup(getSharedPreferences("Characters", Context.MODE_PRIVATE))

        //goes to and searches shared preferences and looks for open position if creating character other wise update already made character
        //LocalStorage.addCharacter(CharacterCreator.createRandom())

        //removes all characters from shared preferences
        //LocalStorage.clearCharacters()

//        println(LocalStorage.characters.size)
//        for(x in LocalStorage.characters)
//            println("\n"+x.uuid)
//        Thread.sleep(1000)
//        LocalStorage.removeCharacter("c5c20522-3523-4bb9-b630-83e76582c3b6")
//        println(LocalStorage.characters.size)
//        for(x in LocalStorage.characters)
//            println("\n"+x.uuid)
    }
}