package com.revature.myapplication

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import java.util.jar.Attributes
import java.lang.Exception
import kotlin.random.Random


class CharacterCreationScreen : Fragment() {

    lateinit var saveBtn:Button
    lateinit var random:Button

    lateinit var nameField:EditText
    lateinit var ageField:EditText
    lateinit var strengthField:EditText
    lateinit var dexterityField:EditText
    lateinit var constitutionField:EditText
    lateinit var intelligenceField:EditText
    lateinit var wisdomField:EditText
    lateinit var charismaField:EditText

    lateinit var raceField:Spinner
    lateinit var classField:Spinner
    lateinit var heightField:Spinner
    lateinit var weightField:Spinner
    lateinit var eyeColorField:Spinner
    lateinit var hairColorField:Spinner

    lateinit var nameCheck:CheckBox
    lateinit var strCheck:CheckBox
    lateinit var dexCheck:CheckBox
    lateinit var conCheck:CheckBox
    lateinit var intCheck:CheckBox
    lateinit var wisCheck:CheckBox
    lateinit var chaCheck:CheckBox
    lateinit var heightCheck:CheckBox
    lateinit var weightCheck:CheckBox
    lateinit var ageCheck:CheckBox
    lateinit var hairCheck:CheckBox
    lateinit var eyeCheck:CheckBox
    lateinit var classCheck:CheckBox
    lateinit var raceCheck:CheckBox

    var classes = arrayOf(
        "Barbarian",
        "Bard",
        "Cleric",
        "Druid",
        "Fighter",
        "Monk",
        "Paladin",
        "Ranger",
        "Rogue",
        "Sorcerer",
        "Warlock",
        "Wizard"
        )

    var heights = arrayOf(
        "small",
        "medium",
        "large")

    var colors = arrayOf(
        "Black",
        "Gray",
        "White",
        "Yellow",
        "Amber",
        "Hazel",
        "Green",
        "Blue",
        "Red",
        "Purple",
        "Deep Blue",
        "Orange",
        "Sea Green",
        "Emerald Green"
    )

    var weights = arrayOf(
        "slim",
        "average",
        "heavy"
    )

    var races = arrayOf("Dragonborn",
    "Hill Dwarf",
    "Mountain Dwarf",
    "High Elf",
    "Wood Elf",
    "Dark Elf",
    "Water Genasi",
    "Earth Genasi",
    "Fire Genasi",
    "Air Genasi",
    "Forest Gnome",
    "Rock Gnome",
    "Goliath",
    "Lightfoot Halfling",
    "Stout Halfling",
    "Half Elf",
    "Half-Orc",
    "Human",
    "Tiefling")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
            // Inflate the layout for this fragment

            //_binding =
            var root:ViewGroup =
                inflater.inflate(R.layout.fragment_character_creation, null) as ViewGroup;

            saveBtn = root.findViewById<Button>(R.id.saveBtn)
            random = root.findViewById<Button>(R.id.randomizeBtn)

            nameField = root.findViewById<EditText>(R.id.editTextName)
            ageField = root.findViewById<EditText>(R.id.editTextAge)
            strengthField = root.findViewById<EditText>(R.id.editTextStrength)
            dexterityField = root.findViewById<EditText>(R.id.editTextDexterity)
            constitutionField = root.findViewById<EditText>(R.id.editTextConstitution)
            intelligenceField = root.findViewById<EditText>(R.id.editTextIntelligence)
            wisdomField = root.findViewById<EditText>(R.id.editTextWisdom)
            charismaField = root.findViewById<EditText>(R.id.editTextCharisma)

            raceField = root.findViewById<Spinner>(R.id.spinnerRace)
            classField = root.findViewById<Spinner>(R.id.spinnerClass)
            heightField = root.findViewById<Spinner>(R.id.spinnerHeight)
            weightField = root.findViewById<Spinner>(R.id.spinnerWeight)
            eyeColorField = root.findViewById<Spinner>(R.id.spinnerEyeColor)
            hairColorField = root.findViewById<Spinner>(R.id.spinnerHairColor)

            nameCheck = root.findViewById<CheckBox>(R.id.checkName)
            strCheck = root.findViewById<CheckBox>(R.id.checkStr)
            dexCheck = root.findViewById<CheckBox>(R.id.checkDex)
            conCheck = root.findViewById<CheckBox>(R.id.checkCon)
            intCheck = root.findViewById<CheckBox>(R.id.checkInt)
            wisCheck = root.findViewById<CheckBox>(R.id.checkWis)
            chaCheck = root.findViewById<CheckBox>(R.id.checkCha)
            heightCheck = root.findViewById<CheckBox>(R.id.checkHeight)
            weightCheck = root.findViewById<CheckBox>(R.id.checkWeight)
            ageCheck = root.findViewById<CheckBox>(R.id.checkAge)
            hairCheck = root.findViewById<CheckBox>(R.id.checkHair)
            eyeCheck = root.findViewById<CheckBox>(R.id.checkEye)
            classCheck = root.findViewById<CheckBox>(R.id.checkClass)
            raceCheck = root.findViewById<CheckBox>(R.id.checkRace)

//<=====================================================Do Not Code Above this line=====================>



        var adapClass: ArrayAdapter<CharSequence> = ArrayAdapter.createFromResource(
            this.requireContext(),
            R.array.class_array,
            android.R.layout.simple_spinner_dropdown_item
        )
        adapClass.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        classField.adapter = adapClass

        var adapRace: ArrayAdapter<CharSequence> = ArrayAdapter.createFromResource(
            this.requireContext(),
            R.array.race_array,
            android.R.layout.simple_spinner_item
        )
        adapRace.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        raceField.adapter = adapRace

        var adapHeight: ArrayAdapter<CharSequence> = ArrayAdapter.createFromResource(
            this.requireContext(),
            R.array.height,
            android.R.layout.simple_spinner_item
        )
        adapHeight.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        heightField.adapter = adapHeight

        var adapWeight: ArrayAdapter<CharSequence> = ArrayAdapter.createFromResource(
            this.requireContext(),
            R.array.weight,
            android.R.layout.simple_spinner_item
        )
        adapWeight.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        weightField.adapter = adapWeight

        var adapColor: ArrayAdapter<CharSequence> = ArrayAdapter.createFromResource(
                this.requireContext(),
                R.array.colors,
                android.R.layout.simple_spinner_item
            )
        adapColor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        eyeColorField.adapter = adapColor
        hairColorField.adapter = adapColor

        loadChar()
            saveBtn.setOnClickListener {

                Log.i("Information",
                    "Name: ${nameField.text}\n" +
                            "Age: ${ageField.text}\n" +
                            "Strength: ${strengthField.text}\n" +
                            "Dexterity: ${dexterityField.text}\n" +
                            "Constitution: ${constitutionField.text}\n" +
                            "Wisdom: ${wisdomField.text}\n" +
                            "Charisma: ${charismaField.text}\n" +
                            "Race: ${raceField.selectedItem}\n" +
                            "Class: ${classField.selectedItem}\n" +
                            "Height: ${heightField.selectedItem}\n" +
                            "Weight: ${weightField.selectedItem}\n" +
                            "Eye Color: ${eyeColorField.selectedItem}\n" +
                            "Hair Color: ${hairColorField.selectedItem}\n"
                    )
                if(validateInput())
                {
                    if(LocalStorage.currentChar == -1)
                        LocalStorage.addCharacter(Character(
                            age = try {
                                ageField.text.toString().toInt()
                            } catch (e:Exception){-1},
                            name = nameField.text.toString(),
                            strength = try{strengthField.text.toString().toInt()} catch (e:Exception){-1},
                            dexterity = try{dexterityField.text.toString().toInt()} catch (e:Exception){-1},
                            constitution = try{constitutionField.text.toString().toInt()} catch (e:Exception){-1},
                            intelligence = try{intelligenceField.text.toString().toInt()} catch (e:Exception){-1},
                            wisdom      = try{wisdomField.text.toString().toInt()} catch (e:Exception){-1},
                            charisma    = try{charismaField.text.toString().toInt()} catch (e:Exception){-1},
                            race    = raceField.selectedItem.toString(),
                            charClass = classField.selectedItem.toString(),
                            height = try{heightField.selectedItem.toString().toInt()} catch (e:Exception){-1},
                            weight = try{weightField.selectedItem.toString().toInt()} catch (e:Exception){-1},
                            eyeColor =eyeColorField.selectedItem.toString(),
                            hairColor = hairColorField.selectedItem.toString(),
                        ))
                    else
                    {
                        var temp = LocalStorage.characters[LocalStorage.currentChar]
                            temp.age = try {
                                ageField.text.toString().toInt()
                            } catch (e:Exception){-1}
                        temp.name = nameField.text.toString()
                        temp.strength = try{strengthField.text.toString().toInt()} catch (e:Exception){-1}
                        temp.dexterity = try{dexterityField.text.toString().toInt()} catch (e:Exception){-1}
                        temp.constitution = try{constitutionField.text.toString().toInt()} catch (e:Exception){-1}
                        temp.intelligence = try{intelligenceField.text.toString().toInt()} catch (e:Exception){-1}
                        temp.wisdom      = try{wisdomField.text.toString().toInt()} catch (e:Exception){-1}
                        temp.charisma    = try{charismaField.text.toString().toInt()} catch (e:Exception){-1}
                        temp.race    = raceField.selectedItem.toString()
                        temp.charClass = classField.selectedItem.toString()
                        temp.height = try{heightField.selectedItem.toString().toInt()} catch (e:Exception){-1}
                        temp.weight = try{weightField.selectedItem.toString().toInt()} catch (e:Exception){-1}
                        temp.eyeColor =eyeColorField.selectedItem.toString()
                        temp.hairColor = hairColorField.selectedItem.toString()
                    }
                    findNavController().navigate(R.id.action_characterCreationScreen_to_home2)
                }

                else
                {
                    Toast.makeText(requireContext(), "Input Failure", Toast.LENGTH_LONG).show()
                    Log.i("Information", "INVALID INPUT")
                }
            }
            random.setOnClickListener {
                RandomChar()
            }

        return root
    }
    fun loadChar()
    {
        if(LocalStorage.currentChar == -1)
        {
            RandomChar()
        }
        else
        {
            var myCharacter = LocalStorage.characters[LocalStorage.currentChar]
            nameField.setText(myCharacter.name)
            ageField.setText(myCharacter.age.toString())
            strengthField.setText(myCharacter.strength.toString())
            dexterityField.setText(myCharacter.dexterity.toString())
            constitutionField.setText(myCharacter.constitution.toString())
            wisdomField.setText(myCharacter.wisdom.toString())
            intelligenceField.setText(myCharacter.intelligence.toString())
            charismaField.setText(myCharacter.charisma.toString())
            raceField.setSelection(races.indexOf(myCharacter.race))
            weightField.setSelection(weights.indexOf(myCharacter.weight))
            heightField.setSelection(heights.indexOf(myCharacter.height))
            println("Index of the class:  "+classes.indexOf(myCharacter.charClass))
            classField.setSelection(classes.indexOf(myCharacter.charClass))
            eyeColorField.setSelection(colors.indexOf(myCharacter.eyeColor))
            hairColorField.setSelection(colors.indexOf(myCharacter.hairColor))
        }
    }

    fun validateInput():Boolean
    {
        if(nameField.text.toString().length<2)return false
        if(ageField.text.toString().toInt() < 5)return false
        if(strengthField.text.toString().toInt()<8)return false
        if(dexterityField.text.toString().toInt()<8)return false
        if(constitutionField.text.toString().toInt()<8)return false
        if(wisdomField.text.toString().toInt()<8)return false
        if(charismaField.text.toString().toInt()<8)return false
        if(intelligenceField.text.toString().toInt()<8)return false
        if(raceField.selectedItemPosition==-1)return false
        if(classField.selectedItemPosition==-1)return false
        if(heightField.selectedItemPosition==-1)return false
        if(weightField.selectedItemPosition==-1)return false
        if(eyeColorField.selectedItemPosition==-1)return false
        if(hairColorField.selectedItemPosition==-1)return false
        return true
    }

    fun RandomChar(){

        if(!nameCheck.isChecked)
            nameField.setText(CharacterCreator.createRandom().name)
        if(!strCheck.isChecked)
            strengthField.setText(CharacterCreator.createRandom().strength.toString())
        if(!dexCheck.isChecked)
            dexterityField.setText(CharacterCreator.createRandom().dexterity.toString())
        if(!conCheck.isChecked)
            constitutionField.setText(CharacterCreator.createRandom().constitution.toString())
        if(!intCheck.isChecked)
            intelligenceField.setText(CharacterCreator.createRandom().intelligence.toString())
        if(!wisCheck.isChecked)
            wisdomField.setText(CharacterCreator.createRandom().wisdom.toString())
        if(!chaCheck.isChecked)
            charismaField.setText(CharacterCreator.createRandom().charisma.toString())
        if(!ageCheck.isChecked)
            ageField.setText(CharacterCreator.createRandom().age.toString())
        if(!classCheck.isChecked)
            classField.setSelection(Random.nextInt(0,classes.size - 1))
        if(!raceCheck.isChecked)
            raceField.setSelection(Random.nextInt(0, races.size - 1))
        if(!heightCheck.isChecked)
            heightField.setSelection(Random.nextInt(0, heights.size - 1))
        if(!weightCheck.isChecked)
            weightField.setSelection(Random.nextInt(0, weights.size - 1))
        if(!hairCheck.isChecked)
            hairColorField.setSelection(Random.nextInt(0, colors.size - 1))
        if(!eyeCheck.isChecked)
            eyeColorField.setSelection(Random.nextInt(0, colors.size - 1))

    }
}
