package com.revature.myapplication

import android.content.SharedPreferences
import java.util.*

object LocalStorage {
    lateinit var sharedPreferences:SharedPreferences

    var currentChar = -1
    var characters:MutableList<Character> = mutableListOf<Character>()

    fun addCharacter(char:Character):Boolean {
         var ed:SharedPreferences.Editor = sharedPreferences.edit()

        if(characters.size<6 )//|| removeCharacter(char.uuid.toString()))
                for (x in 1..5) {
                    if (sharedPreferences.contains("char$x uuid")) {
                        if (sharedPreferences.getString("char$x uuid", "") == char.uuid.toString()) {
                            ed.putString("char$x uuid", char.uuid.toString())
                            ed.putString("char$x name", char.name)
                            ed.putInt("char$x strength", char.strength)
                            ed.putInt("char$x dexterity", char.dexterity)
                            ed.putInt("char$x constitution", char.constitution)
                            ed.putInt("char$x intelligence", char.intelligence)
                            ed.putInt("char$x wisdom", char.wisdom)
                            ed.putInt("char$x charisma", char.charisma)
                            ed.putString("char$x background", char.background)
                            ed.putString("char$x hairColor", char.hairColor)
                            ed.putInt("char$x height", char.height)
                            ed.putInt("char$x bodyType", char.bodyType)
                            ed.putString("char$x eyeColor", char.eyeColor)
                            ed.putString("char$x charClass", char.charClass)
                            ed.putInt("char$x weight", char.weight)
                            ed.putInt("char$x age", char.age)
                            ed.putString("char$x race", char.race)
                            ed.commit()
                            //removeCharacter(char.uuid.toString())
                            characters.add(char)
                            return true
                        }
                    }else
                    {
                        ed.putString("char$x uuid", char.uuid.toString())
                        ed.putString("char$x name", char.name)
                        ed.putInt("char$x strength", char.strength)
                        ed.putInt("char$x dexterity", char.dexterity)
                        ed.putInt("char$x constitution", char.constitution)
                        ed.putInt("char$x intelligence", char.intelligence)
                        ed.putInt("char$x wisdom", char.wisdom)
                        ed.putInt("char$x charisma", char.charisma)
                        ed.putString("char$x background", char.background)
                        ed.putString("char$x hairColor", char.hairColor)
                        ed.putInt("char$x height", char.height)
                        ed.putInt("char$x bodyType", char.bodyType)
                        ed.putString("char$x eyeColor", char.eyeColor)
                        ed.putString("char$x charClass", char.charClass)
                        ed.putInt("char$x weight", char.weight)
                        ed.putInt("char$x age", char.age)
                        ed.putString("char$x race", char.race)
                        ed.commit()
                        characters.add(char)
                        return true
                    }
                }
        return false
    }
    fun removeCharacter(id:String):Boolean
    {

        for(x in 1..5) {
            if(sharedPreferences.contains("char$x uuid"))
            {
                if(sharedPreferences.getString("char$x uuid", "")==id)
                {
                    var ed:SharedPreferences.Editor = sharedPreferences.edit()
                    ed.remove("char$x uuid")
                    ed.remove("char$x name")
                    ed.remove("char$x strength")
                    ed.remove("char$x dexterity")
                    ed.remove("char$x constitution")
                    ed.remove("char$x intelligence")
                    ed.remove("char$x wisdom")
                    ed.remove("char$x charisma")
                    ed.remove("char$x background")
                    ed.remove("char$x hairColor")
                    ed.remove("char$x height")
                    ed.remove("char$x bodyType")
                    ed.remove("char$x eyeColor")
                    ed.remove("char$x charClass")
                    ed.remove("char$x weight")
                    ed.remove("char$x age")
                    ed.remove("char$x race")
                    ed.commit()
                    characters.removeAt(characters.indexOfFirst { it.uuid.toString()==id })
                    return true
                }
            }
        }
        return false
    }
    fun setup(sp:SharedPreferences)
    {
        sharedPreferences = sp
        if(characters.size>0)
            return
        for(x in 1..5)
        {

            if(sp.contains("char$x uuid"))
            {

                characters.add(
                        Character(
                            uuid = sp.getString("char$x uuid", UUID.randomUUID().toString())!!,
                            name            =sp.getString("char$x name", "$%^&*")!!,
                            strength        =sp.getInt("char$x strength", -1),
                            dexterity       =sp.getInt("char$x dexterity", -1),
                            constitution    =sp.getInt("char$x constitution", -1),
                            intelligence    =sp.getInt("char$x intelligence", -1),
                            wisdom          =sp.getInt("char$x wisdom", -1),
                            charisma        =sp.getInt("char$x charisma", -1),
                            background      =sp.getString("char$x background", "Drop In")!!,
                           // hairColor       =sp.getString("char$x hairColor", "TRANSPARENT")!!,
                            height          =sp.getInt("char$x height", -1),
                            bodyType        =sp.getInt("char$x bodyType", -1),
       //                     eyeColor        =sp.getString("char$x eyeColor", "TRANSPARENT")!!,
                            charClass       =sp.getString("char$x charClass", "None")!!,
                            weight          =sp.getInt("char$x weight", -1),
                            age             =sp.getInt("char$x age", -1),
                            race            =sp.getString("char$x race", "Useless")!!,
                        )
                )
            }
        }

    }
    fun clearCharacters()
    {
        for(x in 1..5) {
            if(sharedPreferences.contains("char$x uuid"))
            {
                var ed:SharedPreferences.Editor = sharedPreferences.edit()
                ed.remove("char$x uuid")
                ed.remove("char$x name")
                ed.remove("char$x strength")
                ed.remove("char$x dexterity")
                ed.remove("char$x constitution")
                ed.remove("char$x intelligence")
                ed.remove("char$x wisdom")
                ed.remove("char$x charisma")
                ed.remove("char$x background")
                ed.remove("char$x hairColor")
                ed.remove("char$x height")
                ed.remove("char$x bodyType")
                ed.remove("char$x eyeColor")
                ed.remove("char$x charClass")
                ed.remove("char$x weight")
                ed.remove("char$x age")
                ed.remove("char$x race")
                ed.commit()
            }
        }
        characters.clear()
    }

}