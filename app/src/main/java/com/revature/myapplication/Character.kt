package com.revature.myapplication

import android.graphics.Color
import java.util.*

data class Character constructor(
    val uuid: String = UUID.randomUUID().toString(),
    var name:String="$%^&*",
    var strength:Int=-1,
    var dexterity:Int=-1,
    var constitution:Int=-1,
    var intelligence:Int=-1,
    var wisdom:Int=-1,
    var charisma:Int = -1,
    var background:String="Drop In",
    var hairColor: String="BLACK",
    var height:Int=-1,
    var bodyType:Int=-1,
    var eyeColor: String="BLACK",
    var charClass:String = "None",
    var weight:Int = -1,
    var age: Int = -1,
    var race:String = "Useless"
    )
